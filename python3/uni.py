import argparse
from inspect import getmembers, isfunction
import locale
import os
import os.path
import re
from typing import Callable, cast

def run_test(name: str, func: Callable[[str, argparse.Namespace], None], args: argparse.Namespace) -> None:
    for fil in os.listdir("../testcases"):
        contents = ""
        with open(os.path.join("../testcases/", fil)) as fh:
            contents = fh.read()

        print("Running test \"{}\" on testcases/{}".format(name, fil))
        print("====================")
        func(contents, args)
        print("")

def init() -> argparse.Namespace:
    locale.setlocale(locale.LC_ALL, "")

    parser = argparse.ArgumentParser(description="Run some unicode tests")
    parser.add_argument("-v", dest="verbose", action="store_true", help="Run tests in verbose mode")
    parser.add_argument("-t", dest="tests", help="Name tests to use, comma-separated")
    parser.add_argument("mod", nargs='?', help="Test module to run")
    return parser.parse_args()

def main() -> None:
    args = init()

    mods = []   # type: List[module]
    if args.mod:
        modfile = "{}.py".format(args.mod)
        if re.match(r"^[a-zA-Z0-9]+$", args.mod) and os.path.isfile(modfile):
            mods.append(__import__(args.mod))
        else:
            print("No matching module")
    else:
        for fil in os.listdir():
            if fil != __file__ and os.path.isfile(fil) and re.match(r"^[a-zA-Z0-9]+\.py$", fil):
                fil = re.sub(r"\.py$", "", fil)
                mods.append(__import__(fil))

    for mod in mods:
        if len(mods) > 1 or args.mod:
            print("Running {}.py\n".format(mod.__name__))
        for f in getmembers(mod):
            if not args.tests or f[0] in map(lambda x: x.strip(), args.tests.split(",")):
                if isfunction(f[1]) and not "__" in f[0]:
                    run_test(f[0], cast(Callable[[str, argparse.Namespace], None], f[1]), args)

if __name__ == "__main__":
    main()
