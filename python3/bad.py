from argparse import Namespace
from typing import Callable

len_ = len

def len(contents: str, args: Namespace) -> None:
    for line in contents.splitlines():
        print("strlen({}): {}".format(line, len_(line)))


def cmp(contents: str, args: Namespace) -> None:
    for line in contents.splitlines():
        if " " in line:
            (first, second) = line.split(" ")
            print("strcmp({}, {}): {}".format(first, second, first == second))
        else:
            if args.verbose:
                print("Nothing to compare to, skipping")


def __charclass(func: Callable[[str], bool], contents: str) -> None:
    for line in contents.splitlines():
        for char in line:
            print("{}({}): {}".format(func.__name__, char, func(char)))


def alpha(contents: str, args: Namespace) -> None:
    __charclass(str.isalpha, contents)


def num(contents: str, args: Namespace) -> None:
    __charclass(str.isdigit, contents)


def space(contents: str, args: Namespace) -> None:
    __charclass(str.isspace, contents)


def conv(contents: str, args: Namespace) -> None:
    for line in contents.splitlines():
        try:
            val = str(int(line))
        except ValueError as e:
            val = str(e)
        print("strtoll({}): {}".format(line, val))
