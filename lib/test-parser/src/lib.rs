extern crate libc;
extern crate yaml_rust;

use std::boxed::Box;
use std::ffi::{CString, CStr, OsStr};
use std::fs::File;
use std::io::{self, Read};
use std::mem;
use std::os::unix::ffi::OsStrExt;
use std::path::Path;
use yaml_rust::{Yaml, YamlLoader};

#[derive(Debug)]
pub struct Test {
    args: Vec<String>,
    description: String,
}

#[derive(Debug)]
pub struct TestCases {
    tests: Vec<Test>,
}

#[derive(Debug)]
pub enum TestCaseError {
    Io(io::Error),
    ScanError(yaml_rust::ScanError),
    Misc(String),
}

impl TestCaseError {
    fn from_str(msg: &'static str) -> Self {
        TestCaseError::Misc(String::from(msg))
    }
}

pub type TestCaseResult = Result<TestCases, TestCaseError>;

fn str_to_yaml(name: &'static str) -> Yaml {
    Yaml::String(String::from(name))
}

fn yaml_to_string(yml: &Yaml, name: &'static str) -> Result<String, TestCaseError> {
    Ok(String::from(try!(yml.as_str()
        .ok_or(TestCaseError::Misc(format!("{} is not a string!", name))))))
}

fn get_test(test: &Yaml) -> Result<Test, TestCaseError> {
    let top_level = try!(test.as_hash()
        .ok_or(TestCaseError::from_str("test top level must be a hash")));

    let test_str = str_to_yaml("test");
    let test_hash = try!(try!(top_level.get(&test_str)
            .ok_or(TestCaseError::from_str("test key is required for all testcases")))
        .as_hash()
        .ok_or(TestCaseError::from_str("test must be a hash")));

    let desc = str_to_yaml("description");
    let args = str_to_yaml("args");

    let args_res_vec = try!(try!(test_hash.get(&args)
            .ok_or(TestCaseError::from_str("Missing args")))
        .as_vec()
        .ok_or(TestCaseError::from_str("args is not an array")));

    let mut args_vec = Vec::new();
    for arg in args_res_vec {
        args_vec.push(try!(yaml_to_string(arg, "args")));
    }

    Ok(Test {
        description: try!(yaml_to_string(try!(test_hash.get(&desc).ok_or(
                    TestCaseError::from_str("Missing description")
            )),
                                         "description")),
        args: args_vec,
    })
}

fn extract_testcases(testcases: &Vec<Yaml>) -> TestCaseResult {
    let mut ret = TestCases { tests: Vec::new() };
    for testcase in testcases {
        match get_test(testcase) {
            Ok(test) => ret.tests.push(test),
            Err(e) => return Err(e),
        }
    }

    Ok(ret)
}

pub fn load_yaml(filename: &Path) -> TestCaseResult {
    let mut file = try!(File::open(filename).map_err(TestCaseError::Io));
    let mut contents = String::new();

    // Populate file contents
    try!(file.read_to_string(&mut contents).map_err(TestCaseError::Io));

    let yaml_list = try!(YamlLoader::load_from_str(contents.as_str())
        .map_err(TestCaseError::ScanError));
    let ref yaml = yaml_list[0];
    let top_level = try!(yaml.as_hash()
        .ok_or(TestCaseError::Misc(String::from("top level must be a hash"))));

    match top_level.get(&Yaml::String(String::from("testcases"))) {
        Some(arr) => {
            extract_testcases(try!(arr.as_vec()
                .ok_or(TestCaseError::from_str("testcases must be a list"))))
        }
        None => Err(TestCaseError::from_str("'testcases' must be a list of tests")),
    }
}

#[repr(C)]
struct CTest {
    description: *mut libc::c_char,
    args: *mut *mut libc::c_char,
    num_args: libc::size_t,
}

#[repr(C)]
pub struct CTestCases {
    tests: *mut *mut CTest,
    num_tests: libc::size_t,
}

#[no_mangle]
pub extern "C" fn testcases_load_yaml(filename: *const libc::c_char,
                                      testcases: *mut *mut CTestCases)
                                      -> libc::c_int {
    let path = Path::new(OsStr::from_bytes(unsafe { CStr::from_ptr(filename).to_bytes() }));

    let rusty_testcases = match load_yaml(path) {
        Ok(rtcs) => rtcs,
        Err(_) => return 1,
    };

    let mut ctestcases: Vec<*mut CTest> = rusty_testcases.tests
        .into_iter()
        .map(|rtestcase| {
            let num_args = rtestcase.args.len();
            let mut args = rtestcase.args
                .into_iter()
                .map(|arg| CString::new(arg).unwrap().into_raw())
                .collect::<Vec<*mut libc::c_char>>();
            let argsptr = args.as_mut_ptr();
            mem::forget(args);

            Box::into_raw(Box::new(CTest {
                description: CString::new(rtestcase.description).unwrap().into_raw(),
                num_args: num_args,
                args: argsptr,
            }))
        })
        .collect();

    let ctcptr = ctestcases.as_mut_ptr();
    let ret = Box::new(CTestCases {
        tests: ctcptr,
        num_tests: ctestcases.len(),
    });
    mem::forget(ctestcases);
    unsafe { *testcases = Box::into_raw(ret) };

    0
}

#[cfg(test)]
mod tests {
    use std::env;
    use super::*;

    #[test]
    fn basic() {
        let dir = match env::current_dir() {
            Ok(x) => x,
            Err(_) => return,
        };

        let testcase = dir.join("../../testcases/01.yml");
        let testcase_res = load_yaml(testcase.as_path());
        assert!(testcase_res.is_ok());

        let testcase = testcase_res.unwrap();
        assert_eq!(testcase.tests[0].description,
                   String::from("control with one arg"));
    }
}
