#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

void
len(char *contents, size_t length, const struct opts *o)
{
	char *str;

	for (str = strtok(contents, "\n"); str; str = strtok(NULL, "\n")) {
		printf("strlen(%s): %zu\n", str, strlen(str));
	}
}

void
cmp(char *contents, size_t length, const struct opts *o)
{
	char *str;
	char *first, *second;
	char *loc, *sec_end;

	first = alloca(2048); second = alloca(2048);
	for (str = strtok(contents, "\n"); str; str = strtok(NULL, "\n")) {
		if (! (loc = strchr(str, ' '))) {
			if (o->verbose)
				printf("Nothing to compare %s to...skipping\n", str);

			continue;
		}

		if (! (sec_end = strchr(str, '\n')))
			if (! (sec_end = strchr(str, '\0'))) {
				if (o->verbose)
					printf("Couldn't find end of %s\n", str);
			}

		first = memcpy(first, str, loc - str);
		first[loc-str] = '\0';
		second = memcpy(second, loc + 1, sec_end - loc + 1);
		second[sec_end-loc+1] = '\0';
		printf("strcmp(%s, %s): %d\n", first, second, strcmp(first, second));
	}
}

void
alpha(char *contents, size_t length, const struct opts *o)
{
	char *str;
	unsigned long i;

	for (str = strtok(contents, "\n"); str; str = strtok(NULL, "\n")) {
		for (i = 0; i < strlen(str); i++) {
			printf("isalpha(%c): %d\n", str[i], isalpha(str[i]));
		}
	}
}

void
num(char *contents, size_t length, const struct opts *o)
{
	char *str;
	unsigned long i;

	for (str = strtok(contents, "\n"); str; str = strtok(NULL, "\n")) {
		for (i = 0; i < strlen(str); i++) {
			printf("isdigit(%c): %d\n", str[i], isdigit(str[i]));
		}
	}
}

void
space(char *contents, size_t length, const struct opts *o)
{
	char *str;
	unsigned long i;

	for (str = strtok(contents, "\n"); str; str = strtok(NULL, "\n")) {
		for (i = 0; i < strlen(str); i++) {
			printf("isspace(%c): %d\n", str[i], isspace(str[i]));
		}
	}
}

void
conv(char *contents, size_t length, const struct opts *o)
{
	char *str;
	const char *err;
	long long result;

	for (str = strtok(contents, "\n"); str; str = strtok(NULL, "\n")) {
		errno = 0;
		result = strtoll(str, NULL, 10);
		if (errno)
			printf("strtoll(%s): failed - %s\n", str, strerror(errno));
		else
			printf("strtoll(%s): %lld\n", str, result);

		result = strtonum(str, LLONG_MIN, LLONG_MAX, &err);
		if (err)
			printf("strtonum(%s): failed - %s\n", str, err);
		else
			printf("strtonum(%s): %lld\n", str, result);

	}
}

int
main(int argc, char **argv)
{
	struct opts o = { 0 };

	init(argc, argv, &o);
	test_files("len", len, &o);
	test_files("cmp", cmp, &o);
	test_files("alpha", alpha, &o);
	test_files("num", num, &o);
	test_files("space", space, &o);
	test_files("conv", conv, &o);

	return 0;
}
