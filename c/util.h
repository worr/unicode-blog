#ifndef __UTIL_H
#define __UTIL_H

#include <stdbool.h>
#include <sys/types.h>

#ifdef __linux__
#include <bsd/stdlib.h>
#endif

/* RUST */
struct test {
	char *description;
	char **args;
	size_t num_args;
};

struct testcases {
	struct test **tests;
	size_t num_tests;
};

int testcases_load_yaml(const char *filename, struct testcases **testcases);

/* END RUST */

struct opts {
	bool verbose;
	char *test;
};

void test_files(const char *, void (*) (char *, size_t, const struct opts *), const struct opts *);
void init(int, char **, struct opts *);
void *xmalloc(size_t);

#endif
