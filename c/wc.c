#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <wctype.h>

#include "util.h"

void
len(char *contents, size_t length, const struct opts *o)
{
	char *str;
	wchar_t wstr[2048];


	for (str = strtok(contents, "\n"); str; str = strtok(NULL, "\n")) {
		mbstowcs(wstr, str, sizeof(wstr));
		printf("wcslen(%ls): %zu\n", wstr, wcslen(wstr));
	}
}

void
cmp(char *contents, size_t length, const struct opts *o)
{
	char *str;
	char first[2048] = { 0 }, second[2048] = { 0 };
	wchar_t first_w[2048] = { 0 }, second_w[2048] = { 0 };
	char *loc, *sec_end;

	for (str = strtok(contents, "\n"); str; str = strtok(NULL, "\n")) {
		if (! (loc = strchr(str, ' '))) {
			if (o->verbose)
				printf("Nothing to compare %s to...skipping\n", str);

			continue;
		}

		if (! (sec_end = strchr(str, '\n')))
			if (! (sec_end = strchr(str, '\0'))) {
				if (o->verbose)
					printf("Couldn't find end of %s\n", str);
			}

		memcpy(first, str, loc - str);
		first[loc-str] = '\0';
		mbstowcs(first_w, first, sizeof(first_w));

		memcpy(second, loc + 1, sec_end - loc + 1);
		second[sec_end-loc+1] = '\0';
		mbstowcs(second_w, second, sizeof(second_w));
		printf("wcscmp(%ls, %ls): %d\n", first_w, second_w, wcscmp(first_w, second_w));
	}
}

void
alpha(char *contents, size_t length, const struct opts *o)
{
	char *str;
	unsigned long i;
	wchar_t wstr[2048];

	for (str = strtok(contents, "\n"); str; str = strtok(NULL, "\n")) {
		mbstowcs(wstr, str, sizeof(wstr));
		for (i = 0; i < wcslen(wstr); i++) {
			printf("iswalpha(%lc): %d\n", wstr[i], iswalpha(wstr[i]));
		}
	}
}

void
num(char *contents, size_t length, const struct opts *o)
{
	char *str;
	unsigned long i;
	wchar_t wstr[2048];

	for (str = strtok(contents, "\n"); str; str = strtok(NULL, "\n")) {
		mbstowcs(wstr, str, sizeof(wstr));
		for (i = 0; i < wcslen(wstr); i++) {
			printf("iswdigit(%lc): %d\n", wstr[i], iswdigit(wstr[i]));
		}
	}
}

void
space(char *contents, size_t length, const struct opts *o)
{
	char *str;
	unsigned long i;
	wchar_t wstr[2048];

	for (str = strtok(contents, "\n"); str; str = strtok(NULL, "\n")) {
		mbstowcs(wstr, str, sizeof(wstr));
		for (i = 0; i < wcslen(wstr); i++) {
			printf("iswspace(%lc): %d\n", wstr[i], iswspace(wstr[i]));
		}
	}
}

void
conv(char *contents, size_t length, const struct opts *o)
{
	char *str;
	const char *err;
	long long result;
	wchar_t wstr[2048];

	for (str = strtok(contents, "\n"); str; str = strtok(NULL, "\n")) {
		mbstowcs(wstr, str, sizeof(wstr));
		errno = 0;
		result = wcstoll(wstr, NULL, 10);
		if (errno)
			printf("wcstoll(%ls): failed - %s\n", wstr, strerror(errno));
		else
			printf("wcstoll(%ls): %lld\n", wstr, result);
	}
}

int
main(int argc, char **argv)
{
	struct opts o = { 0 };

	init(argc, argv, &o);
	test_files("len", len, &o);
	test_files("cmp", cmp, &o);
	test_files("alpha", alpha, &o);
	test_files("num", num, &o);
	test_files("space", space, &o);
	test_files("conv", conv, &o);

	return 0;
}
