#include <dirent.h>
#include <err.h>
#include <fcntl.h>
#include <getopt.h>
#include <locale.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "util.h"

static void
file_contents(int dfd, const char *dirpath, const char *filename, void *contents, size_t len)
{
	int fd;
	struct testcases *tc;
	char *fullpath;

	if (strstr(filename, ".yml")) {
		if (!(asprintf(&fullpath, "%s/%s", dirpath, filename)))
			err(1, "asprintf");

		testcases_load_yaml(fullpath, &tc);
		printf("%s: %zu\n", fullpath, tc->num_tests);
		printf("%s: %s\n", fullpath, tc->tests[0]->description);
		printf("%s: %s\n", fullpath, tc->tests[1]->description);
		free(fullpath);
		return;
	}

	memset(contents, 0, len);
	if ((fd = openat(dfd, filename, O_RDONLY)) == -1)
		err(1, "openat");

	if (read(fd, contents, len) == -1)
		err(1, "read");

	(void) close(fd);
}

void
test_files(const char *name, void (*func) (char *, size_t, const struct opts *), const struct opts *o)
{
	DIR *dir;
	int dfd;
	struct dirent *dent;
	struct stat sat;
	char buffer[2048] = { 0 };

	if (o->test && strcmp(o->test, name))
		return;

	if (! (dir = opendir("../testcases")))
		err(1, "opendir");

	dfd = dirfd(dir);
	while ((dent = readdir(dir))) {
		if (fstatat(dfd, dent->d_name, &sat, AT_SYMLINK_NOFOLLOW))
			err(1, "fstatat");

		if (S_ISREG(sat.st_mode)) {
			file_contents(dfd, "../testcases", dent->d_name, buffer, sizeof(buffer));

			printf("Running test \"%s\" on testcases/%s\n", name, dent->d_name);
			puts("====================");
			func(buffer, sizeof(buffer), o);
			puts("");
		}
	}

	(void) closedir(dir);
}

static void
usage(void)
{
	fprintf(stderr, "Usage: %s [-v] [-t testname]\n", getprogname());
	fprintf(stderr, "\t-v\tRun in verbose mode\n");
	fprintf(stderr, "\t-t\tRun `testname` and only `testname`\n");
	exit(1);
}

void
init(int argc, char **argv, struct opts *o)
{
	int ch;

	setlocale(LC_ALL, "");
	setprogname(argv[0]);

	while ((ch = getopt_long(argc, argv, "ht:v", NULL, NULL)) != -1) {
		switch (ch) {
			case 'v':
				o->verbose = true;
				break;
			case 't':
				o->test = strdup(optarg);
				break;
			case 'h':
			default:
				usage();
		}
	}
}

void *
xmalloc(size_t size)
{
	void *ret;

	if (! (ret = malloc(size)))
		err(1, "malloc");
	return ret;
}
