# Vim 7.4.1030 (and neovim 8f22031)
* Hangul from individual Jamo characters is treated as the number of Jamo in the character

# MobaXterm v8.2
* Hangul composed of Jamo doesn't render the same as the precomposed alternative (font bug? happens with multiple fonts)

# less (458) on FreeBSD
* despite locale returning:

        LANG=en_US.UTF-8
        LC_CTYPE="en_US.UTF-8"
        LC_COLLATE="en_US.UTF-8"
        LC_TIME="en_US.UTF-8"
        LC_NUMERIC="en_US.UTF-8"
        LC_MONETARY="en_US.UTF-8"
        LC_MESSAGES="en_US.UTF-8"
        LC_ALL=en_US.UTF-8

    and LESSCHARSET being set, less still renders codepoints instead of
    unicode characters
