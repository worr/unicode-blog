# Unicode Examples

This repo contains all of the example codes for an upcoming blogpost about
Unicode.

## Organization

* bugs.md - list of bugs found in software used developing this blogpost
* links.md - useful links / inspiration
* testcases - text files containing testcases
* lang-specific implementations
    * c
        * util.c - utility functions for setting up test harnesses
        * bad.c - uses str* functions in stdlib (build and run this)
        * wc.c - uses wc* functions in stdlib (build and run this)
    * golang
        * bad/main.go - uses naïve functions in stdlib (go run this)
        * rune/main.go - uses rune-based functions in stdlib (go run this)
        * normalized/main.go - normalizes, and then uses functions from rune (go run this)
        * util/util.go - utility functions for setting up test harnesses
    * python2
        * bad.py - uses stdlib functions
        * uni.py - harness that runs all tests in all files (run this)
    * python3
        * bad.py - uses stdlib functions
        * normalized.py - normalizes input then runs stdlib functions
        * uni.py - harness that runs all tests in all files (run this)
