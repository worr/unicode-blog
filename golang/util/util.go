package util

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
)

type TestFunc func(string, Opts)

type Opts struct {
	Verbose  bool
	Testname string
}

func TestFiles(f TestFunc, opts Opts) {
	err := filepath.Walk("../testcases", func(path string, info os.FileInfo, err error) error {
		st, err := os.Stat(path)
		switch {
		case err != nil:
			return err
		case ! st.Mode().IsRegular():
			return nil
		}

		rawcontents, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		contentsbuff := bytes.NewBuffer(rawcontents)
		contents := contentsbuff.String()

		// Get pointer of function f, then use runtime to get the name of the function
		fullname := runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name()
		name := strings.SplitN(fullname, ".", 2)[1]

		if opts.Testname != "" && opts.Testname != name {
			return nil
		}

		fmt.Printf("Running test \"%v\" on %v\n", name, path)
		fmt.Printf("====================\n")
		f(contents, opts)
		fmt.Printf("\n")

		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
}

func Init() Opts {
	opts := Opts{false, ""}
	flag.BoolVar(&opts.Verbose, "verbose", false, "enable verbose output")
	flag.StringVar(&opts.Testname, "test", "", "test to run")
	flag.Parse()

	return opts
}
