package main

import (
	"fmt"
	"gitlab.com/worr/unicode-blog/golang/util"
	"strconv"
	"strings"
	"unicode"
)

func len_(contents string, opts util.Opts) {
	for _, v := range strings.Split(contents, "\n") {
		if v != "" {
			fmt.Printf("len(%v): %v\n", v, len(v))
		}
	}
}

func cmp(contents string, opts util.Opts) {
	for _, v := range strings.Split(contents, "\n") {
		if v != "" {
			pieces := strings.SplitN(v, " ", 2)
			if len(pieces) != 2 {
				continue
			}

			fmt.Printf("cmp(%v, %v): %v\n", pieces[0], pieces[1], pieces[0] == pieces[1])
		}
	}
}

func alpha(contents string, opts util.Opts) {
	for _, v := range strings.Split(contents, "\n") {
		if v != "" {
			for i := 0; i < len(v); i++ {
				fmt.Printf("isalpha(%c): %v\n", v[i], unicode.IsLetter(rune(v[i])))
			}
		}
	}
}

func num(contents string, opts util.Opts) {
	for _, v := range strings.Split(contents, "\n") {
		if v != "" {
			for i := 0; i < len(v); i++ {
				fmt.Printf("isdigit(%c): %v\n", v[i], unicode.IsDigit(rune(v[i])))
			}
		}
	}
}

func space(contents string, opts util.Opts) {
	for _, v := range strings.Split(contents, "\n") {
		if v != "" {
			for i := 0; i < len(v); i++ {
				fmt.Printf("isspace(%c): %v\n", v[i], unicode.IsSpace(rune(v[i])))
			}
		}
	}
}

func conv(contents string, opts util.Opts) {
	for _, v := range strings.Split(contents, "\n") {
		if v != "" {
			if num, err := strconv.Atoi(string(v)); err == nil {
				fmt.Printf("strtoll(%v): %v\n", v, num)
			} else {
				fmt.Printf("strtoll(%v): %v\n", v, err)
			}
		}
	}
}

func main() {
	opts := util.Init()

	util.TestFiles(len_, opts)
	util.TestFiles(cmp, opts)
	util.TestFiles(alpha, opts)
	util.TestFiles(num, opts)
	util.TestFiles(space, opts)
	util.TestFiles(conv, opts)
}
