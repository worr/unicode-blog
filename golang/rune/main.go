package main

import (
	"fmt"
	"gitlab.com/worr/unicode-blog/golang/util"
	"strconv"
	"strings"
	"unicode"
)

func len_(contents string, opts util.Opts) {
	for _, v := range strings.Split(contents, "\n") {
		if v != "" {
			fmt.Printf("len(%v): %v\n", v, len([]rune(v)))
		}
	}
}

func cmp(contents string, opts util.Opts) {
	for _, v := range strings.Split(contents, "\n") {
		if v != "" {
			pieces := strings.SplitN(v, " ", 2)
			if len(pieces) != 2 {
				continue
			}

			fmt.Printf("cmp(%v, %v): %v\n", pieces[0], pieces[1], pieces[0] == pieces[1])
		}
	}
}

func alpha(contents string, opts util.Opts) {
	for _, v := range strings.Split(contents, "\n") {
		if v != "" {
			for _, ru := range v {
				fmt.Printf("isalpha(%c): %v\n", ru, unicode.IsLetter(ru))
			}
		}
	}
}

func num(contents string, opts util.Opts) {
	for _, v := range strings.Split(contents, "\n") {
		if v != "" {
			for _, ru := range v {
				fmt.Printf("isdigit(%c): %v\n", ru, unicode.IsDigit(ru))
			}
		}
	}
}

func space(contents string, opts util.Opts) {
	for _, v := range strings.Split(contents, "\n") {
		if v != "" {
			for _, ru := range v {
				fmt.Printf("isspace(%c): %v\n", ru, unicode.IsSpace(ru))
			}
		}
	}
}

func conv(contents string, opts util.Opts) {
	for _, v := range strings.Split(contents, "\n") {
		if v != "" {
            if num, err := strconv.Atoi(v); err == nil {
                fmt.Printf("strtoll(%v): %v\n", v, num)
            } else {
                fmt.Printf("strtoll(%v): %v\n", v, err)
            }
		}
	}
}

func main() {
	opts := util.Init()

	util.TestFiles(len_, opts)
	util.TestFiles(cmp, opts)
	util.TestFiles(alpha, opts)
	util.TestFiles(num, opts)
	util.TestFiles(space, opts)
	util.TestFiles(conv, opts)
}
