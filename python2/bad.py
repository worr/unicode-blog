from __future__ import print_function
len_ = len

def len(contents, args):
    for line in contents.splitlines():
        print("strlen({}): {}".format(line, len_(line)))


def cmp(contents, args):
    for line in contents.splitlines():
        if " " in line:
            (first, second) = line.split(" ")
            print("strcmp({}, {}): {}".format(first, second, first == second))
        else:
            if args.verbose:
                print("Nothing to compare to, skipping")


def __charclass(func, contents):
    for line in contents.splitlines():
        for char in line:
            print("{}({}): {}".format(func.__name__, char, func(char)))


def alpha(contents, args):
    __charclass(str.isalpha, contents)


def num(contents, args):
    __charclass(str.isdigit, contents)


def space(contents, args):
    __charclass(str.isspace, contents)


def conv(contents, args):
    for line in contents.splitlines():
        try:
            val = int(line)
        except ValueError as e:
            val = str(e)
        print("strtoll({}): {}".format(line, val))
