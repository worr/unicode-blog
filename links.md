The following were helpful in writing these examples:

* http://eev.ee/blog/2015/09/12/dark-corners-of-unicode/
* http://gernot-katzers-spice-pages.com/var/korean_hangul_unicode.html
* http://unicode.org/reports/tr51/
* http://unicode.org/reports/tr15/
* https://www.mediawiki.org/wiki/Unicode_normalization_considerations
* http://www.unicode.org/Public/8.0.0/ucd/CompositionExclusions.txt
* https://blog.golang.org/normalization
* https://blog.golang.org/strings
